/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker;

import java.util.Vector;
import javax.swing.table.AbstractTableModel;


/**
 *
 * @author nitwit
 * This Class makes a connection with the mysql db and stores the
 * requested data in the cache using a vector.
 *
 */


public class TableModel extends AbstractTableModel{

    /**
     * Creates a new instance of Connection1
     */


  private Vector cache; // will hold String[] objects . . .
  private String[] record;
  private int colCount;
  //private String[] headers;
  private String[] headers = {"Filename", "Extension","Attributes","Reserved","Create time","Create time",
      "Create data(Y:M:D)","Last access date(Y:M:D)","EA-Index","Last mod time(h:m:s)","Last mod date(Y:M:D)",
      "Start of file(cluster)","Size"};

 public TableModel() {
    cache = new Vector();
  }

  public String getColumnName(int i) {
    return headers[i];
  }

  public int getColumnCount() {
    return colCount;
  }

  public int getRowCount() {
    return cache.size();
  }

  public Object getValueAt(int row, int col) {
    return ((String[]) cache.elementAt(row))[col];
  }


  // All the real work happens here; in a real application,
  // we'd probably perform the query in a separate thread.
  public void setQuery(String q) {
        cache = new Vector();
    
        /* while (rs.next()) {
         final String[] record = new String[colCount];
        for (int i = 0; i < colCount; i++) {
          record[i] = rs.getString(i + 1);
        }*/
        cache.addElement(record);
      //}

        //only after complete stuff...
      fireTableChanged(null); // notify everyone that we have a new table.
   // } catch (Exception e) {
      cache = new Vector(); // blank it out and keep going.
    //  e.printStackTrace();
   // }
  }


}