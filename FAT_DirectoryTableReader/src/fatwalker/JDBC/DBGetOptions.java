/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.JDBC;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.DatabaseMetaData;
import com.mysql.jdbc.ResultSetMetaData;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joeri
 */
public class DBGetOptions {
    private String[] dbset = new fatwalker.io.XMLReadFile().GetXMLRecord();
    private String[]    land;
    private ArrayList   al;
    private int colCount;
    private String[] headers;
    private Connection conn;

    /** Creates a new instance of NewDK */
    public String[] DBGetOptions(String option) {
        al = new ArrayList();
        
        Connection conn = null;
        Statement  st   = null;
        ResultSet  rs   = null;
        
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
            st = (Statement) conn.createStatement();
            
            rs = st.executeQuery("SELECT * FROM nand WHERE ID=0 ");
            ResultSetMetaData meta = (ResultSetMetaData) rs.getMetaData();
             colCount = meta.getColumnCount();
            headers = new String[colCount];
            for (int h = 1; h <= colCount; h++) {
             headers[h - 1] = meta.getColumnName(h);
      }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            //log = new Log("Fout bij GetCountry.class " +e.getMessage());
     
        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(st != null){
                    st.close();
                }
                if(conn != null){
                    conn.close();
                }
            }
            catch(SQLException sqle){
                System.out.println(sqle.getMessage());
                //log = new Log("Fout bij afsluiten GetCountry.class "+sqle.getMessage());
            }
        }
       return headers;
    }

    public String[] DBDistinctExtension(String option, String table) {
        al = new ArrayList();

        Connection conn = null;
        Statement  st   = null;
        ResultSet  rs   = null;

        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
            st = (Statement) conn.createStatement();

            rs = st.executeQuery("SELECT DISTINCT Extension FROM "+table+" ORDER BY Extension ASC");

            while(rs.next()){
                al.add(rs.getString("Extension"));
            }
            //Convert ArrayList to String[]
            land = (String[]) al.toArray( new String[al.size()] );

        }
        catch(Exception e){
            System.out.println(e.getMessage());
            //log = new Log("Fout bij GetCountry.class " +e.getMessage());

        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(st != null){
                    st.close();
                }
                if(conn != null){
                    conn.close();
                }
            }
            catch(SQLException sqle){
                System.out.println(sqle.getMessage());
                //log = new Log("Fout bij afsluiten GetCountry.class "+sqle.getMessage());
            }
        }
       return land;
    }

     public int DBPathBuilder(int groupid, String name, String table) {
        al = new ArrayList();

        Connection conn = null;
        Statement  st   = null;
        ResultSet  rs   = null;
        int clusternr = 0;
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
            st = (Statement) conn.createStatement();

            rs = st.executeQuery("SELECT StartCluster FROM `"+table+"` WHERE Filename='"+name+"' AND groupid='"+groupid+"'");

            while(rs.next()){
                clusternr = rs.getInt("StartCluster");
                System.out.println(rs.getInt("StartCluster"));
            }
            //Convert ArrayList to String[]
            //land = (String[]) al.toArray( new String[al.size()] );

        }
        catch(Exception e){
            System.out.println(e.getMessage());
            //log = new Log("Fout bij GetCountry.class " +e.getMessage());

        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(st != null){
                    st.close();
                }
                if(conn != null){
                    conn.close();
                }
            }
            catch(SQLException sqle){
                System.out.println(sqle.getMessage());
                //log = new Log("Fout bij afsluiten GetCountry.class "+sqle.getMessage());
            }
        }
       return clusternr;
    }

     public String[] GetFilename(int clusternr, String table, int newgrID) {
        al = new ArrayList();

        Connection conn = null;
        Statement  st   = null;
        ResultSet  rs   = null;
        
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
            st = (Statement) conn.createStatement();
            System.out.println("NEWGR:"+newgrID);
            rs = st.executeQuery("SELECT Filename, groupid FROM `"+table+"` WHERE StartCluster='"+clusternr+"' AND groupid='"+newgrID+"' ");

            String tmp = new String();
            while(rs.next()){
                tmp = rs.getString("Filename");
                tmp += ";"+rs.getInt("groupid");

                al.add(tmp);
                System.out.println("Filename"+rs.getString("Filename"));
            }
            //Convert ArrayList to String[]
            land = (String[]) al.toArray( new String[al.size()] );

        }
        catch(Exception e){
            System.out.println(e.getMessage());
            //log = new Log("Fout bij GetCountry.class " +e.getMessage());

        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(st != null){
                    st.close();
                }
                if(conn != null){
                    conn.close();
                }
            }
            catch(SQLException sqle){
                System.out.println(sqle.getMessage());
                //log = new Log("Fout bij afsluiten GetCountry.class "+sqle.getMessage());
            }
        }
       return land;
    }



       public int GetGroupID(int clusternr, String table) {
       int groupid = 0;

        Connection conn = null;
        Statement  st   = null;
        ResultSet  rs   = null;

        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
            st = (Statement) conn.createStatement();

            rs = st.executeQuery("SELECT groupid FROM `"+table+"` WHERE Filename='.' AND StartCluster='"+clusternr+"'");

            String tmp = new String();
            while(rs.next()){
                groupid = rs.getInt("groupid");
                System.out.println(rs.getInt("groupid"));
            }
            //Convert ArrayList to String[]

        }
        catch(Exception e){
            System.out.println(e.getMessage());
            //log = new Log("Fout bij GetCountry.class " +e.getMessage());

        }
        finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(st != null){
                    st.close();
                }
                if(conn != null){
                    conn.close();
                }
            }
            catch(SQLException sqle){
                System.out.println(sqle.getMessage());
                //log = new Log("Fout bij afsluiten GetCountry.class "+sqle.getMessage());
            }
        }
        if(groupid == 0){
            return -1;
        }
       return groupid;
    }

    public String[] getTables(){
        String[] result = null;
        try {
            try {
                try {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                } catch (InstantiationException ex) {
                    Logger.getLogger(DBGetOptions.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(DBGetOptions.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DBGetOptions.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                conn = (Connection) DriverManager.getConnection("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
                // st = (Statement) conn.createStatement();
            } catch (SQLException ex) {
                Logger.getLogger(DBGetOptions.class.getName()).log(Level.SEVERE, null, ex);
            }
            // st = (Statement) conn.createStatement();
            // st = (Statement) conn.createStatement();
            java.sql.DatabaseMetaData dmd = conn.getMetaData();
            String[] types = {"TABLE"};
            ResultSet resultSet = null;
            try {
                resultSet = dmd.getTables(null, null, "%", types);
            } catch (SQLException ex) {
                Logger.getLogger(DBGetOptions.class.getName()).log(Level.SEVERE, null, ex);
            }

            resultSet.last();
            int rows = resultSet.getRow();
            System.out.println(rows);
            result = new String[rows];
            resultSet.beforeFirst();
            int count = 0;
            while (resultSet.next()) {
                String tableName = null;
                try {
                   result[count] = resultSet.getString(3);
                   count++;
                } catch (SQLException ex) {
                    Logger.getLogger(DBGetOptions.class.getName()).log(Level.SEVERE, null, ex);
                } // String tableSchema = resultSet.getString(2);
            }
            try {
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(DBGetOptions.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBGetOptions.class.getName()).log(Level.SEVERE, null, ex);
        }
          return result;
    }

}


