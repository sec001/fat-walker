/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.JDBC;

import java.sql.SQLClientInfoException;

/**
 *
 * @author joeri
 */
public class DBConnection {

    ResultTable sql;
    private String[] dbset = new fatwalker.io.XMLReadFile().GetXMLRecord();
    public DBConnection(ResultTable sql){
        this.sql = sql;
    }

    public void OpenConnection(){
        System.out.println("Open connectoin");
        this.sql.initDB("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
    }

    public void submitQuery(String query, String table, String field, String order){ //date_format(Datum, '%d-%m-%y')
        if(query == null){
            this.sql.setQuery("SELECT ID, Filename, Extension, Attribute, CreateRes, CreateTime, CreateDate, AccessDate, ModTime, ModDate, StartCluster, Size,groupid FROM "+table+" ORDER BY "+order+" ASC");
        }
        else{
            this.sql.setQuery("SELECT ID, Filename, Extension, Attribute, CreateRes, CreateTime, CreateDate, AccessDate, ModTime, ModDate, StartCluster, Size,groupid FROM `"+table+"` WHERE `"+field+"`LIKE '%"+query+"%' ORDER BY "+order+" ASC");
        }
    }

    public void CloseConnection(){
        this.sql.closeDB();
    }

}
