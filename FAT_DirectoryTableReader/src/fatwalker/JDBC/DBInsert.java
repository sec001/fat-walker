/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joeri
 */
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
//import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class DBInsert{

    private String      fileEntry;
    private String[] dbset = new fatwalker.io.XMLReadFile().GetXMLRecord();
    //private Log         log;
            Connection conn = null;
        Statement  st   = null;
        ResultSet  rs   = null;

    /** Creates a new instance of NewDK */
    public DBInsert() {
       

    }

    public void doAll(String fileEntry, String table){
        conn = null;
        st   = null;
         rs   = null;

        this.fileEntry = fileEntry;
         String[] entry = fileEntry.split(";");
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
            st = conn.createStatement();
            st.executeUpdate("INSERT INTO "+table+" (Filename, Extension, Attribute, CreateTime, CreateDate, AccessDate, ModTime, ModDate, StartCluster, Size, CreateRes, groupid) " + "VALUES('" + entry[0] + "','" + entry[1] + "','" + entry[2] + "','" + entry[3] + "','" + entry[4] + "','" + entry[5] + "','" + entry[6] + "','" + entry[7] + "'," + entry[8] + "," + entry[9] + "," + entry[10] + "," + entry[11] + ")");
            //System.out.println(fileEntry);
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            //log = new Log("Fout bij NewCountry.class "+e.getMessage());
        }
           finally{
               try{
                if(rs != null){
                    rs.close();
                }
                if(st != null){
                    st.close();
                }
                if(conn != null){
                    conn.close();
                }
            }
            catch(SQLException sqle){
                System.out.println(sqle.getMessage());
                //log = new Log("Fout bij afsluiten NewCountry.class "+sqle.getMessage());
            }
        }
    }


    public void OpenConnection(){


        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://"+dbset[2]+":"+dbset[3]+"/"+dbset[4]+"", dbset[0], dbset[1]);
            st = conn.createStatement();

             
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            //log = new Log("Fout bij NewCountry.class "+e.getMessage());
        }
      
    }

    public void Insert(String fileEntry){
         this.fileEntry = fileEntry;
         String[] entry = fileEntry.split(";");
        try {
            st.executeUpdate("INSERT INTO nand (Filename, Extension, Attribute, CreateTime, CreateDate, AccessDate, ModTime, ModDate, StartCluster, Size, CreateRes, groupid ) " + "VALUES('" + entry[0] + "','" + entry[1] + "','" + entry[2] + "','" + entry[3] + "','" + entry[4] + "','" + entry[5] + "','" + entry[6] + "','" + entry[7] + "'," + entry[8] + "," + entry[9] + "," + entry[10]  +"," + entry[11]+ ")");
        } catch (SQLException ex) {
            Logger.getLogger(DBInsert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void Close(){
               
            try{
                if(rs != null){
                    rs.close();
                }
                if(st != null){
                    st.close();
                }
                if(conn != null){
                    conn.close();
                }
            }
            catch(SQLException sqle){
                System.out.println(sqle.getMessage());
                //log = new Log("Fout bij afsluiten NewCountry.class "+sqle.getMessage());
            }
        }

}

