package fatwalker.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;


/**
 *
 * @author nitwit
 * This Class makes a connection with the mysql db and stores the 
 * requested data in the cache using a vector.
 *
 */


public class ResultTable extends AbstractTableModel{
    
    /**
     * Creates a new instance of Connection1
     */
   
    
  private Vector cache; // will hold String[] objects . . .
  private String[] record;
  private int colCount;
  private String[] headers;
  private Connection db;
  private Statement statement;
  private String currentURL;
  private String s = "Not! connected to database server...";

 public ResultTable() {
    cache = new Vector();
  }

  public String getColumnName(int i) {
    return headers[i];
  }

  public int getColumnCount() {
    return colCount;
  }

  public int getRowCount() {
    return cache.size();
  }

  public Object getValueAt(int row, int col) {
    return ((String[]) cache.elementAt(row))[col];
  }
  

  public void setHostURL(String url) {
    if (url.equals(currentURL)) {
      // same database, we can leave the current connection open
      return;
    }
    // Oops . . . new connection required
    closeDB();
    String user = null;
    String password = null;
    initDB(url,user,password);
    currentURL = url;
  }

  // All the real work happens here; in a real application,
  // we'd probably perform the query in a separate thread.
  public void setQuery(String q) {
    cache = new Vector();
    System.out.println("query binnen...");
    try {
      // Execute the query and store the result set and its metadata
      ResultSet rs = statement.executeQuery(q);
      ResultSetMetaData meta = rs.getMetaData();
      colCount = meta.getColumnCount();
      System.out.println("Colcount: " + colCount);
      // Now we must rebuild the headers array with the new column names
      headers = new String[colCount];
      for (int h = 1; h <= colCount; h++) {
          if(meta.getColumnName(h).equals("date_format(Datum, '%d-%m-%y')")){
             headers[h -1] = "Datum";
          }
          else{
        headers[h - 1] = meta.getColumnName(h);
          }
      }

      // and file the cache with the records from our query. This would
      // not be
      // practical if we were expecting a few million records in response
      // to our
      // query, but we aren't, so we can do this.
      while (rs.next()) {
         final String[] record = new String[colCount];
        for (int i = 0; i < colCount; i++) {
          record[i] = rs.getString(i + 1);
        }
        cache.addElement(record);
      }
      fireTableChanged(null); // notify everyone that we have a new table.
    } catch (Exception e) {
      cache = new Vector(); // blank it out and keep going.
      e.printStackTrace();
    }
  }

    public void initDB(String url,String user, String password) {
    try {

      Class.forName("com.mysql.jdbc.Driver").newInstance();
      db = DriverManager.getConnection(url,user,password);
      statement = db.createStatement( );
      if(!db.isClosed()){
           	s = "Sucessfully Connected to Mysql Server..."; 
      }
               
         	else{
           	s = "Not connected to Mysql Server...";}
    }
    catch(Exception e) {
        
        
        System.out.println("Could not initialize the database.");
      
      e.printStackTrace( );
    }
  }

  public void closeDB() {
    try {
      if (statement != null) {
        statement.close();
      }
      if (db != null) {
        db.close();
      }
    } catch (Exception e) {
      System.out.println("Could not close the current connection.");
      e.printStackTrace();
    }
  }
}