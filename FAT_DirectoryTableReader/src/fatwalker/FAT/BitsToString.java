/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.FAT;

/**
 *
 * @author joeri
 */
public class BitsToString {
    ReverseString revstring = new ReverseString();
    FAT fat;
    public BitsToString(){

    }

     public String ConvertBitsToDate(String bits, int year, int month, int day, FAT fat){
        //System.out.println("Bits:"+bits);
        this.fat = fat;
        String str_day      = revstring.ReverseBitsToString(bits.substring(0,(day)));
        String str_month    = revstring.ReverseBitsToString(bits.substring(day,((day+month))));
        String str_year     = revstring.ReverseBitsToString(bits.substring((day+month),(((day+month)+year))));

        try{
        int y = Integer.parseInt(str_year, 2);
        int m = Integer.parseInt(str_month, 2);
        int d = Integer.parseInt(str_day, 2);

        y = y + fat.BASEYEAR;

        bits = ""+y+"-"+String.format("%02d", m)+"-"+String.format("%02d", d)+"";
        }
        catch(NumberFormatException nfe){
            nfe.getStackTrace();
        }
        return bits;
    }

    public String ConvertBitsToTime(String bits, int hour, int min, int sec){
        String str_sec      = revstring.ReverseBitsToString(bits.substring(0,(sec)));
        String str_min      = revstring.ReverseBitsToString(bits.substring(sec,((sec+min))));
        String str_hour     = revstring.ReverseBitsToString(bits.substring((sec+min),(((sec+min)+hour))));

        try{
        int s = Integer.parseInt(str_sec, 2);
        int m = Integer.parseInt(str_min, 2);
        int h = Integer.parseInt(str_hour, 2);

        bits = ""+String.format("%02d", h)+":"+String.format("%02d", m)+":"+String.format("%02d", (s*2))+"";
        }
        catch(NumberFormatException nfe){
            nfe.printStackTrace();
        }
        return bits;
    }


    public String ConvertBitsToDate(String bits, int year, int month, int day, int biteOrder){



        return bits;
    }

    public String ConvertBitsToTime(String bits, int hour, int min, int sec, int biteOrder){

        return bits;
    }

}
