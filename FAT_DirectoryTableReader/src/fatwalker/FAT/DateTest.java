/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.FAT;

/**
 *
 * @author joeri
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

public class DateTest {

  public boolean isValidDate(String inDate) {

    if (inDate == null)
      return false;

    //set the format to use as a constructor argument
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    if (inDate.trim().length() != dateFormat.toPattern().length())
      return false;

    dateFormat.setLenient(false);

    try {
      //parse the inDate parameter
      dateFormat.parse(inDate.trim());
    }
    catch (ParseException pe) {
      return false;
    }
    return true;
  }

  public boolean isValidTime(String inTime) {

    if (inTime == null)
      return false;

    //set the format to use as a constructor argument
    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    if (inTime.trim().length() != dateFormat.toPattern().length())
      return false;

    dateFormat.setLenient(false);

    try {
      //parse the inDate parameter
      dateFormat.parse(inTime.trim());
    }
    catch (ParseException pe) {
        //System.out.println("Invalid date:"+inTime.trim());
      return false;
    }
    return true;
  }

  public boolean compareDate(String date) throws ParseException{
      DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

      Date d1 = df.parse(date);

      String today = df.format(new java.util.Date());
      Date d2 = df.parse(today);

      if(d1.equals(d2)){
        return true;
      }
      else if(d1.before(d2)){
          return true;
      }
      else{
          return false;
      }
  }

  public static void main(String[] args) {

    DateTest test = new DateTest();

    System.out.println(test.isValidDate("31-10-2006"));
    System.out.println(test.isValidDate("2005-13-27"));
  }
}