/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.FAT;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author joeri
 */


public class FAT {
    public static final int BITSIZE = 8;

    public static final int Hour = 5;
    public static final int Minute = 6;
    public static final int Second = 5;

    public static final int Year = 7;
    public static final int Month = 4;
    public static final int Day = 5;

    public static final int CREATETIME = 0;
    public static final int CREATEDATE = 1;
    public static final int ACCESSDATE = 2;
    public static final int MODIFIEDTIME = 3;
    public static final int MODIFIEDDATE = 4;

    public static final int DIRTABLELENGTH  = 32;
    public static final int FILENAME        = 8;
    public static final int EXTENSTION      = 3;
    public static final int ATTRIBUTES      = 1;
    public static final int RESERVED        = 1;
    public static final int TIME_RESOLUTION = 1;
    public static final int CREATE_TIME     = 2;
    public static final int CREATE_DATE     = 2;
    public static final int LAST_ACCESS_DATE= 2;
    public static final int EA_INDEX        = 2;
    public static final int LAST_MOD_TIME   = 2;
    public static final int LAST_MOD_DATE   = 2;
    public static final int START_OF_FILE   = 2;
    public static final int FILESIZE        = 4;

    //Integer.parseInt("04",16);
    public static final int A1 = 1; //read-only
    public static final int A2 = 2; //hidden
    public static final int A3 = 4; //system
    public static final int A4 = 8; //volume-label
    public static final int A5 = 16; //subdir
    public static final int A6 = 32; //archive
    public static final int A7 = 64; //device
    public static final int A8 = 128; //unused
    public static final int A9 = 15; //0xOF for longFilenames
    public static final int A10 = 0;

    public static final int BASEYEAR = 1980;
    private String fileAttribute, filename, fileExtension,createTime,createTimeFineResol, createDate,
            lastAccessDate,lastModTime,lastModDate;

    private int filesize, startOfFile;

    private BitsToString bitTostring = new BitsToString();
    private String tableheader;
    private int th;
    private byte[] bts;
    public FAT(String tableheader){
        this.tableheader = tableheader;
    }

    public FAT(){
      //this.tableheader = "2E202020202020202020201000";
        this.tableheader = "4632323232323232323232160";
        //ConvertHexStrToByte(this.tableheader);
        //Byte b = new Byte(this.tableheader);
        //byte c = b.decode(this.tableheader);
    }


    public void ConvertHexStrToByte(String hex){
        this.bts = new byte[hex.length()/2];
        for(int i = 0; i < bts.length; i++){
            bts[i] = (byte) Integer.parseInt(hex.substring(2*i, 2*i+2),16);
           // System.out.println(bts[i]);
        }
    }

    public String ConvertBitsToString(String bits, int bitmap){

        switch(bitmap){
            case CREATETIME: bits = bitTostring.ConvertBitsToTime(bits,Hour,Minute,Second); break;
            case CREATEDATE: bits = bitTostring.ConvertBitsToDate(bits, Year, Month, Day,this); break;
        }


        return bits;
    }


    public boolean checkAttribute(int bytes){
        boolean stat = false;
        switch(bytes){
            case A1: setFileAttribute("ReadOnly");stat=true; break;
            case A2: setFileAttribute("Hidden");stat=true; break;
            case A3: setFileAttribute("System");stat=true; break;
            case A4: setFileAttribute("Volume Label");stat=true; break;
            case A5: setFileAttribute("Subdirectory");stat=true; break;
            case A6: setFileAttribute("Archive");stat=true; break;
            case A7: setFileAttribute("Device");stat=true; break;
            case A8: setFileAttribute("Unused");stat=true; break;
           // case A9: setFileAttribute("Long Filename");stat=true; break;
            case A10: setFileAttribute("File"); stat=true; break;
            default: stat=false;break;
        }

        return stat;
    }
    

    public boolean checkForDeletedEntry(int del){
        if(del == 229){
            return true;
        }
        else{
            return false;
        }
    }

    //all different fileAttributes
    public void setFilename(String filename){
        this.filename = filename;
    }

    public String getFilename(){
        return this.filename;
    }

    public void setFileExtension(String fileExtension){
        this.fileExtension = fileExtension;
    }

    public String getFileExtension(){
        return this.fileExtension;
    }
    
    public void setFileAttribute(String fileAttribute){
        this.fileAttribute = fileAttribute;
    }

    public String getFileAttribute(){
        return this.fileAttribute;
    }

    public void setCreateTimeFineResolution(String createTimeFineResol){
        this.createTimeFineResol= createTimeFineResol;
    }

    public String getCreateTimeFineResolution(){
        return this.createTimeFineResol;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getCreateTime(){
        return this.createTime;
    }

    public void setCreateDate(String createDate){
        this.createDate = createDate;
    }

    public String getCreateDate(){
        return this.createDate;
    }

    public void setLastAccessDate(String lastAccessDate){
        this.lastAccessDate = lastAccessDate;
    }

    public String getLastAccessDate(){
        return this.lastAccessDate;
    }

    public void setLastModTime(String lastModTime){
        this.lastModTime = lastModTime;
    }

    public String getLastModTime(){
        return this.lastModTime;
    }

    public void setLastModDate(String lastModDate){
        this.lastModDate = lastModDate;
    }

    public String getLastModDate(){
        return this.lastModDate;
    }


    public void setStartOfFile(int startOfFile){
        this.startOfFile = startOfFile;
    }

    public int getStartOfFile(){
        return this.startOfFile;
    }

    public void setFileSize(int filesize){
        this.filesize = filesize;
    }

    public int getFileSize(){
        return this.filesize;
    }
    

    //headers
    public byte[] getHeaderTable(){
        return this.bts;
    }

    //returns header length of string, 1byte = 2 hex values
    public int getHeaderLength(){
        return (this.tableheader.length()/2);
    }

    //set directory entry table header (string)
    public void setDirEntryTableHeader(String tableheader){
        this.tableheader = tableheader;
    }

    //get directory entry table header as string
    public String getDirEntryTableHeader(){
        return this.tableheader;
    }

    public String getDirEntryTableHeaderBinary(){
        return this.tableheader;
    }

    public boolean checkPrintableChars(String input) {

        Pattern printableChars = Pattern.compile("[^\\p{Upper}||\\p{Digit}||\\p{Blank}||\\p{Punct}]");

        Matcher matcher = printableChars.matcher(input);

        boolean garbage = false;
        while (matcher.find()) {
               garbage = true;
        }
        return garbage;
    }

    public boolean checkPrintableFileExtension(String input) {

        Pattern printableChars = Pattern.compile("[^\\p{Upper}||\\p{Digit}||\\p{Blank}]");

        Matcher matcher = printableChars.matcher(input);

        boolean garbage = false;
        while (matcher.find()) {
               garbage = true;
        }
        return garbage;
    }

    
    public String filterGarbage(String tag) {
		String filtered_tag = tag.replaceAll("[^\\p{Upper}||\\p{Digit}||\\p{Blank}||\\p{Punct}]", "");

		return filtered_tag;
    }


}
