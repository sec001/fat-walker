/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.FAT;

/**
 *
 * @author joeri
 */
public class ReverseString {



    public String ReverseBitsToString(String str){
        int len = str.length();
        StringBuffer dest = new StringBuffer(len);
        
        for(int i = (len - 1); i >= 0; i-- ){
            dest.append(str.charAt(i));
        }

        return dest.toString();
    }

}
