/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.FAT;

/**
 *
 * @author joeri
 */
import fatwalker.JDBC.DBInsert;
import fatwalker.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Stack;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class Parser {
    private FAT fat;
    private FileReader reader;
    private ReverseString revstring = new ReverseString();
    private int hcounter; //header counter
    private String buffer;
    private byte[] bts;
    private DBInsert insert;

    private String filename2     = "";
    private String extension     = "";
    private String attribute     = "";
    private String reservered    = "";
    private String time_resol    = "";
    private String create_time   = "";
    private String create_date   = "";
    private String lastAccessDate = "";
    private String modTime = "";
    private String modDate = "";
    private String filesize = "";
    private String clusterNmbr = "";
    private int fileCluster,fileSize;
    private int time;

    private boolean loop;
    private Vector cache;
    private int[] byte_buffer;
    private DateTest dateTest;
    private int EntryCounter = 0;
    private boolean READBOUNDRY = false;
    private boolean cdir = false;
    private boolean pdir = false;
    private String filenameEntry;
    private Stack<String> stack;
    private int groupid = 0;

    public Parser(String filename, String table) throws IOException, ParseException{
        reader = new FileReader(filename);
        reader.ReadFile();
        fat = new FAT();
        insert = new DBInsert();
        dateTest = new DateTest();
        stack = new Stack<String>();
        hcounter = 0;
        buffer = "";
        int invalidEntry = 0;
        System.out.println("Headerlength["+fat.getHeaderLength()+"]");
        int counter = 0;
        int tmp_counter = 0;
        int cnt_cdir = 0;
        byte_buffer = new int[fat.DIRTABLELENGTH];
        boolean dirTable = false;
        EntryCounter = 0;
        while(reader.getByte() != -1){
                    counter++;
                   //loop as long as he find valid entries
                   
                        //only check on boundary edges...optionally
                       for(int buf = 0; buf < fat.DIRTABLELENGTH; buf++){
                           if(buf == 0){
                            byte_buffer[buf] = reader.getLastReadByte();
                           }
                           else{
                            byte_buffer[buf] = reader.getByte();
                           }
                       }         
                    dirTable = checkForDirEntry();

                   if(dirTable){
                        if(fat.getFilename().trim().equals(".")){
                            groupid++;
                            cnt_cdir = counter;
                            cdir = true;
                            filenameEntry = filenameEntry+";"+groupid;
                           // System.out.println(filenameEntry);
                            stack.add(filenameEntry);
                            //System.out.println("Currentdir Found: "+counter);
                        }
                        else if(fat.getFilename().trim().equals("..") && cnt_cdir == (counter -1)){
                            //System.out.println("Parentdir Found:"+counter);
                            pdir = true;
                            filenameEntry = filenameEntry+";"+groupid;
                            stack.add(filenameEntry);
                        }
                        
                       // System.out.println(counter);
                        //System.out.println(filenameEntry);
                        else if(cdir == true && pdir == true ){
                            //if(cnt_cdir != (counter -1)){
                              filenameEntry = filenameEntry+";"+groupid;
                              stack.add(filenameEntry);
                              invalidEntry = 0;
                          //  }
                            //System.out.println("kom hier oook");
                        }
                        else{
                            if(invalidEntry == 0){
                                groupid++;
                                filenameEntry = filenameEntry+";"+groupid;
                                stack.add(filenameEntry);
                            }
                            
                        }
   
                    }
                    else{
                        invalidEntry++;
                        if(invalidEntry == 8){
                            cdir = false;
                            pdir = false;
                            invalidEntry = 0;
                        }

                        while (!stack.empty()) {
                                //System.out.println("Parser:"+stack.pop());
                            insert.doAll(stack.pop(), table);
                            }
                    }

        }
        System.out.println("Entries parsed:["+EntryCounter+"]");
        reader.CloseFile();
//        new JOptionPane.showMessageDialog(null, "File is being parsed, this could take a while.");
        JOptionPane.showMessageDialog(null, "File has been progressed.");

        //insert.Close();
    }


    public boolean checkForDirEntry() throws ParseException{
       
        boolean stat  = false;
        boolean stat1 = false;
        //System.out.println(byte_buffer[11]);
        if(fat.checkAttribute(byte_buffer[11])){
            stat = true;
            //fat.setFileAttribute(attribute);
        }
        create_time = CheckTime(14,16);
        create_date = CheckDate(16,18);
        lastAccessDate = CheckDate(18,20);
        modTime = CheckTime(22,24);
        modDate = CheckDate(24,26);

        String tmp_cl, tmp_cl2;

        tmp_cl = Integer.toHexString(byte_buffer[26]);
        tmp_cl2 = Integer.toHexString(byte_buffer[27]);

        if(tmp_cl.length() == 1){
            tmp_cl = "0".concat(tmp_cl);
        }
        if(tmp_cl2.length() == 1){
            tmp_cl2 = "0".concat(tmp_cl2);
        }

        //clusterNmbr = tmp_cl.concat(tmp_cl2);
        clusterNmbr = tmp_cl2.concat(tmp_cl);
        fileCluster = Integer.parseInt(clusterNmbr, 16);


        //System.out.println(create_date);
        //&& dateTest.isValidTime(create_time)
        //System.out.println(create_date);
        if(dateTest.isValidDate(create_date) && dateTest.isValidTime(create_time)){
            stat1 = true;
            //System.out.println(stat);
            fat.setCreateDate(create_date);
            fat.setCreateTime(create_time);
            String tmp_fn = "";
            String tmp_fex = "";
            char[] ch = new char[8];
            char[] ex = new char[3];
            for(int j = 0; j < 11; j++){
                if(j < 8){
               tmp_fn += (char)byte_buffer[j];
                }
                else{
                   tmp_fex += (char)byte_buffer[j];
                }
            }
            //System.out.println(tmp_fn);
            time_resol = Integer.toHexString(byte_buffer[13]);
            if(time_resol.length() == 1){
                time_resol = "0".concat(time_resol);
            }

            time = Integer.parseInt(time_resol,16);
           // if(time > -1 && time < 200){
                
           // }
           // else{
            //    stat1 = false;
           // }

            if(!fat.checkPrintableChars(tmp_fn) && !fat.checkPrintableFileExtension(tmp_fex) ){
                fat.setFilename(tmp_fn);
                fat.setFileExtension(tmp_fex);
            }
            else {
                if(fat.checkForDeletedEntry(byte_buffer[0])){
                    //stat1 = true;
                    if(!fat.checkPrintableChars(tmp_fn.substring(1,tmp_fn.length()))){
                        fat.setFilename("?"+tmp_fn.substring(1,tmp_fn.length()));
                        fat.setFileAttribute("Deleted");
                        fat.setFileExtension(tmp_fex);
                    }
                    else{
                        stat1=false;
                    }
                }
                else{
                    stat1 = false;
                }
            }


        String[] tmp_fs = new String[4];
        int counter = 0;
        for(int i = 28; i < 32; i++){
            tmp_fs[counter] = Integer.toHexString(byte_buffer[i]);
            if(tmp_fs[counter].length() == 1){
                tmp_fs[counter] = "0".concat(tmp_fs[counter]);
                //System.out.println(tmp_fs[counter]);
            }
                    counter++;
        }

        filesize = tmp_fs[3].concat(tmp_fs[2]).concat(tmp_fs[1]).concat(tmp_fs[0]);
        //System.out.println("Filename="+fat.getFilename()+"Size:"+filesize);
        try{
                fileSize = Integer.parseInt(filesize, 16);
                //System.out.println(Integer.parseInt(filesize, 16));
        }
        catch(NumberFormatException nfe){
            //nfe.printStackTrace();
            System.out.println(fat.getFilename()+";"+fat.getCreateDate());
            stat1 = false;
        }

        }
        else{
            stat1 = false;
            // System.out.println(stat+"/"+stat1);
        }

        if(stat && stat1 && dateTest.compareDate(create_date)){
            stat = true;
            EntryCounter++;

            filenameEntry = fat.getFilename()+";"+fat.getFileExtension()+";"+fat.getFileAttribute()+";"+fat.getCreateTime()+";"+fat.getCreateDate()+";"+lastAccessDate+";"+modTime+";"+modDate+";"+fileCluster+";"+fileSize+";"+time;

           // System.out.println(fat.getFilename()+"|"+fat.getFileExtension()+"|"+fat.getFileAttribute()+"|"+fat.getCreateTime()+"|"+fat.getCreateDate()
           //         +"|"+lastAccessDate+"|"+modTime+"|"+modDate+"|"+fileCluster+"|"+fileSize);
        }
        else{
            stat = false;
        }
            
        return stat;
    }

    public String CheckDate(int begin, int end){
        create_date = "";
         for(int m = begin; m < end; m++){
               String tmp = Integer.toBinaryString(byte_buffer[m]);
               String final_tmp = "";
               if(tmp.length() < 8){
                    for(int p = 0; p < (fat.BITSIZE-tmp.length()); p++){
                        final_tmp += "0";
                    }
                    final_tmp += tmp;
               }
               else{
                   final_tmp = tmp;
               }
               create_date += revstring.ReverseBitsToString(final_tmp);

         }
         create_date = fat.ConvertBitsToString(create_date, fat.CREATEDATE);
         
         return create_date;
    }


    public String CheckTime(int begin, int end){
        create_time = "";
         for(int m = begin; m < end; m++){

                               String tmp = Integer.toBinaryString(byte_buffer[m]);
                               String final_tmp = "";
                               if(tmp.length() < 8){
                                    for(int p = 0; p < (fat.BITSIZE-tmp.length()); p++){
                                        final_tmp += "0";
                                    }
                                    final_tmp += tmp;
                               }
                               else{
                                   final_tmp = tmp;
                               }

                               create_time += revstring.ReverseBitsToString(final_tmp);

                           }
              create_time = fat.ConvertBitsToString(create_time, fat.CREATETIME);
              return create_time;
    }


    public void TableEntryBuffer(){

    }

    



}
