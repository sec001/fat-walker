/*
 * ColorColumns.java
 *
 * Created on August 9, 2007, 10:52 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package fatwalker;

/*
 * ColorColumns.java
 *
 * Created on August 7, 2007, 8:31 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

class ColorColumns extends DefaultTableCellRenderer{
 
		
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean cellHasFocus, int row, int column) {
			if(column < 4){
                       	setText((table.getModel().getValueAt(row, column)).toString());
				if (isSelected) {
					 setBackground(table.getSelectionBackground());
					// setForeground(table.getSelectionForeground());
				}
				else {
                                        setBackground(new Color(235,234,219));
				}
                        }
                    else if (column > 3 && column < 9) {
				setText((table.getModel().getValueAt(row, column)).toString());
				if (isSelected) {
					 setBackground(table.getSelectionBackground());
					// setForeground(table.getSelectionForeground());
				}
				else {
					setBackground(new Color(188,204,216));
				}
			} else if (column >= 9){
				setText((table.getModel().getValueAt(row, column)).toString());
				if (isSelected) {
					 setBackground(table.getSelectionBackground());
					// setForeground(table.getSelectionForeground());
				}
				else {
                                    setBackground(new Color(235,234,219));
				}
			}
			
			return this;
		}
}
