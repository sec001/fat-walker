/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.io;

/**
 *
 * @author joeri
 */
import fatwalker.FAT.FAT;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;



public class FileReader {
    private BufferedInputStream fis = null;
    private String filename;
    private File file;

    int bytes;
    FAT fat = new FAT();


    public FileReader(String filename){
        this.filename = filename;
        this.file = new File(this.filename);
    }


   //return size of file (convert from long to int)
   public int getFileSize(){
        return (int)this.file.length();
    }
   

   //return byte for byte
   public int getByte() throws IOException{
        int g = fis.read();
        setLastReadByte(g);

        return g;
    }

    //set last read byte
    public void setLastReadByte(int bytes){
        this.bytes = bytes;
    }

    //return last read byte
    public int getLastReadByte(){
        return this.bytes;
    }

    //open file
    public void ReadFile(){
        try{
            //InputStream is = new FileInputStream(this.file);
             fis = new BufferedInputStream(new FileInputStream(this.file));

             //print out length of file in bytes
             System.out.println("Length: ["+(int)this.file.length()+"] bytes");

        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
    }

    //close file
    public void CloseFile() throws IOException{
        fis.close();
    }


}


//mysql statements