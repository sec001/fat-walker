/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fatwalker.io;

/**
 *
 * @author joeri
 */
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;



public class XMLReadFile {
    private NodeList nList;
    private String[] columnNames = {"username","password","server","port","dbname"};
    private String nodeName, nodeName2;

    public XMLReadFile(String filename, String element, String nodeName) throws ParserConfigurationException, SAXException, IOException{
        this.nodeName = nodeName;
        File fXmlFile = new File(filename);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();

        System.out.println("Root element: [" + doc.getDocumentElement().getNodeName()+"]");
        nList = doc.getElementsByTagName(element);
        System.out.println("-----------------------");
        System.out.println("Amount of elements: ["+nList.getLength()+"]");
       
    }

    public XMLReadFile() {
        try {
            this.nodeName = "";
            File fXmlFile = new File("database.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: [" + doc.getDocumentElement().getNodeName() + "]");
            nList = doc.getElementsByTagName("settings");
            System.out.println("-----------------------");
            System.out.println("Amount of elements: [" + nList.getLength() + "]");
        } catch (SAXException ex) {
            Logger.getLogger(XMLReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int getRowCount(){
        return nList.getLength();
    }


        public String[] GetXMLRecord(){
         String[] record = new String[columnNames.length];
        // for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(0);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

              Element eElement = (Element) nNode;

              for(int i = 0; i < columnNames.length; i++){

 
                     record[i] = getTagValue(columnNames[i],eElement,0);
                  }

              }
         return record;
    }

    private static String getTagValue(String sTag, Element eElement, int item){
       try{
        NodeList nlList= eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        //NodeList nlList = eElement.getChildNodes();
        Node nValue = (Node) nlList.item(0);
            return nValue.getNodeValue();
        }
       catch(NullPointerException npe){
            return "";
       }
        
    }

    public static void main(String[] args){
       
            XMLReadFile xmlrf = new XMLReadFile();
           String[] output = xmlrf.GetXMLRecord();
           // System.out.println(xmlrf.GetXMLRecord("username;password;server;port;dbname").length);
           for(int j =0; j < output.length; j++){
               System.out.println(output[j]);
           }

       
    }
}




