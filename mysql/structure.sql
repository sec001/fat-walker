-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2010 at 08:00 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6-1+lenny8


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fat_walker`
--
CREATE DATABASE `fat_walker` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE fat_walker;

-- --------------------------------------------------------

--
-- Table structure for table `nand`
--

CREATE TABLE IF NOT EXISTS "nand" (
  "ID" int(11) NOT NULL,
  "Filename" varchar(8) collate utf8_unicode_ci NOT NULL,
  "Extension" varchar(3) collate utf8_unicode_ci NOT NULL,
  "Attribute" varchar(100) collate utf8_unicode_ci NOT NULL,
  "Reserved" varchar(100) collate utf8_unicode_ci default NULL,
  "CreateRes" int(100) default NULL,
  "CreateTime" time NOT NULL,
  "CreateDate" date NOT NULL,
  "AccessDate" date NOT NULL,
  "EA-Index" varchar(100) collate utf8_unicode_ci default NULL,
  "ModTime" time NOT NULL,
  "ModDate" date NOT NULL,
  "StartCluster" int(100) NOT NULL,
  "Size" int(100) NOT NULL,
  "groupid" int(100) NOT NULL,
  PRIMARY KEY  ("ID")
) AUTO_INCREMENT=1951 ;

-- --------------------------------------------------------

--
-- Table structure for table `nandnospare`
--

CREATE TABLE IF NOT EXISTS "nandnospare" (
  "ID" int(11) NOT NULL,
  "Filename" varchar(8) collate utf8_unicode_ci NOT NULL,
  "Extension" varchar(3) collate utf8_unicode_ci NOT NULL,
  "Attribute" varchar(100) collate utf8_unicode_ci NOT NULL,
  "Reserved" varchar(100) collate utf8_unicode_ci default NULL,
  "CreateRes" int(100) default NULL,
  "CreateTime" time NOT NULL,
  "CreateDate" date NOT NULL,
  "AccessDate" date NOT NULL,
  "EA-Index" varchar(100) collate utf8_unicode_ci default NULL,
  "ModTime" time NOT NULL,
  "ModDate" date NOT NULL,
  "StartCluster" int(100) NOT NULL,
  "Size" int(100) NOT NULL,
  "groupid" int(100) NOT NULL,
  PRIMARY KEY  ("ID")
) AUTO_INCREMENT=2684 ;

-- --------------------------------------------------------

--
-- Table structure for table `nor`
--

CREATE TABLE IF NOT EXISTS "nor" (
  "ID" int(11) NOT NULL,
  "Filename" varchar(8) collate utf8_unicode_ci NOT NULL,
  "Extension" varchar(3) collate utf8_unicode_ci NOT NULL,
  "Attribute" varchar(100) collate utf8_unicode_ci NOT NULL,
  "Reserved" varchar(100) collate utf8_unicode_ci default NULL,
  "CreateRes" int(100) default NULL,
  "CreateTime" time NOT NULL,
  "CreateDate" date NOT NULL,
  "AccessDate" date NOT NULL,
  "EA-Index" varchar(100) collate utf8_unicode_ci default NULL,
  "ModTime" time NOT NULL,
  "ModDate" date NOT NULL,
  "StartCluster" int(100) NOT NULL,
  "Size" int(100) NOT NULL,
  "groupid" int(100) NOT NULL,
  PRIMARY KEY  ("ID")
) AUTO_INCREMENT=693 ;
